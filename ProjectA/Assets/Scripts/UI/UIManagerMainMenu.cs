using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UIManagerMainMenu : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject LoadGame;
    public GameObject Options;
    public GameObject Exit;
    public EventSystem eventSystem;
    public GameObject DefaultSelected;

    public AudioClip HoverButton;
    public AudioClip ClickButton;
    private AudioSource AudioSource;

    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        DefaultSelected = eventSystem.firstSelectedGameObject;
    }

    public void ButtonGame()
    {
        SceneManager.LoadScene("ProvesScene");
    }
    public void ButtonLoadGame()
    {
        MainMenu.SetActive(false);
        LoadGame.SetActive(true);
        GameObject child = LoadGame.transform.GetChild(0).gameObject;
        eventSystem.SetSelectedGameObject(child);
    }
    public void ButtonOptions()
    {
        MainMenu.SetActive(false);
        Options.SetActive(true);
        GameObject child = Options.transform.GetChild(0).gameObject;
        eventSystem.SetSelectedGameObject(child);
    }
    public void ButtonExit()
    {
        Exit.SetActive(true);
        GameObject child = Exit.transform.GetChild(1).gameObject;
        eventSystem.SetSelectedGameObject(child);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
   
    public void DesactivatePannel(GameObject pannelToDesactivate)
    {
        MainMenu.SetActive(true);
        pannelToDesactivate.SetActive(false);
        eventSystem.SetSelectedGameObject(DefaultSelected);
    }
    public void PlayHoverButton()
    {
        AudioSource.PlayOneShot(HoverButton);
    }
    public void PlayClickButton()
    {
        AudioSource.PlayOneShot(ClickButton);
    }
}
