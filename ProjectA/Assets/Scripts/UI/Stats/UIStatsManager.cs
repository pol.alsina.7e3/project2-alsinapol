using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIStatsManager : MonoBehaviour
{
    public PlayerDataSO player;

    public TextMeshProUGUI textName;
    public TextMeshProUGUI numberHp;
    public TextMeshProUGUI numberAttack;
    public TextMeshProUGUI numberDef;
    public TextMeshProUGUI numberAp;

    void Start()
    {
        SetStats();
    }
    public void SetStats()
    {
        textName.text = player.name;
        numberHp.text = player.maxHealth.ToString();
        numberAttack.text = player.Attack.ToString();
        numberDef.text = player.Defense.ToString();
        numberAp.text = player.powerAllomantic.ToString();
    }
}
