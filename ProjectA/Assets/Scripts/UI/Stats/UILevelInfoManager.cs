using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevelInfoManager : MonoBehaviour
{
    public PlayerDataSO playerData;
    Slider levelProgressBar;
    public TextMeshProUGUI currentLevelText;
    public TextMeshProUGUI expToLevelUp;
    void Start()
    {
        levelProgressBar = GetComponentInChildren<Slider>();
        SetTextLevel();
        SetProgressBarLevel();
    }
    public void SetProgressBarLevel()
    {       

        levelProgressBar.minValue = 0;
        levelProgressBar.maxValue = playerData.experienceLeftToLevelUp;
        levelProgressBar.value = playerData.experience;
    }
    public void SetTextLevel()
    {
        currentLevelText.text = "Level: " + playerData.currentLevel.ToString();
        expToLevelUp.text = playerData.experience.ToString() + " / " + playerData.experienceLeftToLevelUp.ToString();
    }


}
