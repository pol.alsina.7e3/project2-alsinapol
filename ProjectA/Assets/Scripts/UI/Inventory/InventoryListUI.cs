using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryListUI : MonoBehaviour
{

    public ListItemsSO listItems;
    TextMeshProUGUI[] infoItem;
    public GameObject slot;
    private List<GameObject> listItemsCreated;
    public EventSystem EventSystem;
    void Start()
    {
        for (int i = 0; i < listItems.items.Count; i++)
        {
            if (listItems.items[i] != null)
            {
                var obj = Instantiate(slot);
                obj.transform.parent = slot.transform.parent;
                setComponentsInfoItem(i, obj);
                obj.GetComponent<ShowInfoItem>().SetItemInfo(listItems.items[i]);
                obj.SetActive(true);    
                //listItemsCreated.Add(obj.transform.parent.GetChild(i).gameObject);
            }
        }
        EventSystem.SetSelectedGameObject(slot.transform.parent.GetChild(1).gameObject);
    }

    void setComponentsInfoItem(int index, GameObject obj)
    {
        infoItem = obj.GetComponentsInChildren<TextMeshProUGUI>();
        infoItem[0].text = listItems.items[index].Name;
        infoItem[1].text = listItems.items[index].QuantityHeld.ToString() + "x";
    }
    
}

