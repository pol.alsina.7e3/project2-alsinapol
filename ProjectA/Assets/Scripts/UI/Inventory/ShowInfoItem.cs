using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ShowInfoItem : MonoBehaviour, ISelectHandler
{
    public TextMeshProUGUI nameItem;
    public Image iconItem;
    public TextMeshProUGUI descriptionItem;
    ItemSo itemInfo;
    public void SetItemInfo(ItemSo item)
    {
        itemInfo = item;
    }
    public void OnSelect(BaseEventData eventData)
    {
        SetInfoItem();
    }
    public void SetInfoItem()
    {
        nameItem.text = itemInfo.Name;
        iconItem.sprite = itemInfo.Icon;
        descriptionItem.text = itemInfo.ItemEffect;
    }
}
