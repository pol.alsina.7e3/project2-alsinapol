using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevelUpManager : MonoBehaviour
{
    public TextMeshProUGUI pointsText;
    public PlayerDataSO playerDataSO;

    [Header("TextStats")]
    public TextMeshProUGUI[] textStats;

    void Start()
    {
        SetPointsLevelText();
    }
    public void SetPointsLevelText()
    {
        pointsText.text = playerDataSO.statsPoints.ToString();
    }
    void RestPointLevelAndUpdate()
    {
        playerDataSO.statsPoints--;
        SetPointsLevelText();
    }
    void PlusPointLevelAndUpdate()
    {
        playerDataSO.statsPoints++;
        SetPointsLevelText();
    }
    bool CheckPoints()
    {
        if (playerDataSO.statsPoints > 0)
        {
            return true;
        }
        return false;
    }
    public void PointUp(int index)
    {
        if (CheckPoints())
        {
            int textNum = int.Parse(textStats[index].text);
            Debug.Log(textNum);
            textNum++;
            textStats[index].text = textNum.ToString();
            RestPointLevelAndUpdate();
        }
    }
    public void PointDown(int index)
    {
        int textNum = int.Parse(textStats[index].text);
        if (textNum > 0)
        {
            textNum--;
            textStats[index].text = textNum.ToString();
            PlusPointLevelAndUpdate();
        }
    }
    public void AcceptButton()
    {
        playerDataSO.BoostHp(int.Parse(textStats[0].text));
        playerDataSO.BoostAttack(int.Parse(textStats[1].text));
        playerDataSO.BoostDefense(int.Parse(textStats[2].text));
        playerDataSO.BoostAP(int.Parse(textStats[3].text));
        for (int i = 0; i < textStats.Length; i++)
        {
            textStats[i].text = "0";
        }
        GameObject.Find("Stats").GetComponent<UIStatsManager>().SetStats();
    }


}
