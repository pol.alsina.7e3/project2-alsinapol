using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowDroppedItemsUI : MonoBehaviour
{
    TextMeshProUGUI nameItemText;
    string nameItem;
    public void SetNameItem(string name)
    {
        nameItem = name;
    }
    private void Start()
    {
        nameItemText = GetComponentInChildren<TextMeshProUGUI>();
        nameItemText.text = nameItem;
        Destroy(this.gameObject, 2f);
    }
}
