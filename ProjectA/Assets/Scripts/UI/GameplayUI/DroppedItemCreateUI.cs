using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItemCreateUI : MonoBehaviour
{
    public GameObject instantiateObject;
    private void OnEnable()
    {
        DropItemsSO.droppedItemUIShow += CreateDroppedItemUI;
    }
    private void OnDisable()
    {
        DropItemsSO.droppedItemUIShow -= CreateDroppedItemUI;
    }
    void CreateDroppedItemUI(string itemName)
    {
        var obj = Instantiate(instantiateObject);
        obj.transform.parent = transform;
        obj.AddComponent<ShowDroppedItemsUI>().SetNameItem(itemName);
        //obj.GetComponent<ShowDroppedItemsUI>().SetNameItem(itemName);
        obj.SetActive(true);
    }
}
