using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UIManagerGameplay : MonoBehaviour
{
    int index;
    public GameObject[] DefaultSelected = new GameObject[5];
    //pannels
    public GameObject[] pannels = new GameObject[5];
    public TextMeshProUGUI[] pannelTextActive = new TextMeshProUGUI[5];
    public Color32 pannelTextColorActive;
    private Color32 defaultTextColor;
    public GameObject staticUI;
    public GameObject HUDD;
    //EventSystem
    public EventSystem eventSystem;

    public AudioClip hoverButtonClip;
    private AudioSource AudioSource;

    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        InputManager.Instance.inputs.GamePlay.Pause.performed += ActivatePauseGameplay;
        InputManager.Instance.inputs.UI.ChangePanelRight.performed += ChangeIndex;
        InputManager.Instance.inputs.UI.ChangePannelLeft.performed += ChangeIndex;
        InputManager.Instance.inputs.UI.ExitPause.performed += ExitPause;
    }
    private void OnDisable()
    {
        InputManager.Instance.inputs.GamePlay.Pause.performed -= ActivatePauseGameplay;
        InputManager.Instance.inputs.UI.ChangePanelRight.performed -= ChangeIndex;
        InputManager.Instance.inputs.UI.ChangePannelLeft.performed -= ChangeIndex;
        InputManager.Instance.inputs.UI.ExitPause.performed -= ExitPause;
        //InputManager.Instance.inputs.UI.Disable();
        //Time.timeScale = 1f;
    }
    void ActivatePauseGameplay(InputAction.CallbackContext obj)
    {
        HUDD.SetActive(false);
        InputManager.Instance.inputs.GamePlay.Disable();
        InputManager.Instance.inputs.UI.Enable();
        Time.timeScale = 0.0f;
        index = 0;
        ChangePannel();
        staticUI.SetActive(true);
    }
    void ChangePannel()
    {        
        pannels[index].SetActive(true);
        if (DefaultSelected[index] != null)eventSystem.SetSelectedGameObject(DefaultSelected[index]);
        defaultTextColor = pannelTextActive[index].color;
        pannelTextActive[index].color = pannelTextColorActive;
        HoverClipPlay();
    }
    void ChangeIndex(InputAction.CallbackContext obj)
    {
        pannelTextActive[index].color = defaultTextColor;
        pannels[index].SetActive(false);
        if (obj.action.name.Equals("ChangePanelRight"))
        {
            index++;
        }
        else
        {
            index--;
        }        
        CheckIndex();
        ChangePannel();
    }
    void CheckIndex()
    {
        if (index < 0)
        {
            index = pannels.Length -1;
        }
        else if (index > pannels.Length -1)
        {
            index = 0;
        }
    }
    void ExitPause(InputAction.CallbackContext obj)
    {
        HUDD.SetActive(true);
        InputManager.Instance.inputs.GamePlay.Enable();
        InputManager.Instance.inputs.UI.Disable();
        Time.timeScale = 1f;
        staticUI.SetActive(false);
        pannelTextActive[index].color = defaultTextColor;
        pannels[index].SetActive(false);
    }
    public void HoverClipPlay()
    {
        AudioSource.PlayOneShot(hoverButtonClip);
    }
}
