using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UIEquipGameplayVial : MonoBehaviour
{
    public ListItemsSO listEquipVials;
    ListItemsSO inventory;
    int index = 0;
    public Image image;
    public TextMeshProUGUI nameItemText;

    private void OnEnable()
    {
        InputManager.Instance.inputs.GamePlay.ChangeConsumableVialUI.performed += ChangeIndex;
        InputManager.Instance.inputs.GamePlay.EatVialConsumable.performed += EatConsumableItem;
    }
    private void OnDisable()
    {
        InputManager.Instance.inputs.GamePlay.ChangeConsumableVialUI.performed -= ChangeIndex;
        InputManager.Instance.inputs.GamePlay.EatVialConsumable.performed -= EatConsumableItem;
    }
    void Start()
    {
        inventory = GameManager.Instance.GetInventoryPlayer();
        SetItemInfoUI();
    }
    void SetItemInfoUI()
    {
        image.sprite = listEquipVials.vialsEquip[index].Icon;
        nameItemText.text = listEquipVials.vialsEquip[index].Name +" " + listEquipVials.vialsEquip[index].QuantityHeld;
    }
    void ChangeIndex(InputAction.CallbackContext obj)
    {
        index++;
        CheckIndex();
        SetItemInfoUI();
    }
    void CheckIndex()
    {
        if (index > listEquipVials.vialsEquip.Count -1)
        {
            index = 0;
        }
    }
    void EatConsumableItem(InputAction.CallbackContext obj)
    {
        listEquipVials.vialsEquip[index].QuantityHeld--;
        nameItemText.text = listEquipVials.vialsEquip[index].Name + " " + listEquipVials.vialsEquip[index].QuantityHeld;
        listEquipVials.vialsEquip[index].ItemEffectExecute();
        CheckItemHeld();
    }
    void CheckItemHeld()
    {
        if (listEquipVials.vialsEquip[index].QuantityHeld <= 0)
        {
            listEquipVials.vialsEquip.Remove(listEquipVials.vialsEquip[index]);
            inventory.items.Remove(listEquipVials.vialsEquip[index]);
        }
    }
}
