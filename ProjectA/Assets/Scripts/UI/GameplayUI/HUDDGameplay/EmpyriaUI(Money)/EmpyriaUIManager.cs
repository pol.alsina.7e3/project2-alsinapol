using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EmpyriaUIManager : MonoBehaviour
{
    public ListItemsSO inventory;
    TextMeshProUGUI EmpyriaText;
    private void Start()
    {
        EmpyriaText = GetComponent<TextMeshProUGUI>();
        EmpyriaText.text = inventory.Empyria.ToString() + "E";
    }
    private void OnEnable()
    {
        ListItemsSO.uiUpdateEmpyria += UpdateEmpyriaText;
    }
    void UpdateEmpyriaText(int empyria)
    {
        EmpyriaText.text = empyria.ToString() + " E";
    }
    private void OnDisable()
    {
        ListItemsSO.uiUpdateEmpyria -= UpdateEmpyriaText;
    }
}
