using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider healthBar;
    public Image fill;
    public Text hpText;
    public void SetMaxHealt(int health)
    {

        healthBar.maxValue = health;
        healthBar.value = health;
        //if (hpText != null) hpText.text = healthBar.value + " / " + healthBar.maxValue;
    }

    public void SetHealth(int health)
    {
        healthBar.value = health;
        //if (hpText != null) hpText.text = healthBar.value + " / " + healthBar.maxValue;
    }
    public void LookAt()
    {
        this.transform.LookAt(Camera.main.transform);
    }
}
