using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateImage : MonoBehaviour
{
    // Start is called before the first frame update
    private int currentButton;
    public bool vial;
    private void OnEnable()
    {
        EquipItem.uiSpriteIconItem += UploadImage;
    }
    private void OnDisable()
    {
        EquipItem.uiSpriteIconItem += UploadImage;
    }
    public void SlotChanger(int i)
    {
        currentButton = i;
    }

    protected virtual void UploadImage(ItemSo i, int j)
    {
        transform.GetChild(currentButton).GetChild(0).GetComponent<Image>().color = new Color(255, 255, 255, 255);
        transform.GetChild(currentButton).GetChild(0).GetComponent<Image>().sprite = i.Icon;      
    }
}
