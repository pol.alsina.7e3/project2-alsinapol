using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EquipManagerUI : MonoBehaviour
{
    public ListItemsSO equip;
    public ListItemsSO listItems;
    public GameObject slotList;
    TextMeshProUGUI[] infoItem;
    public GameObject pannelChooseItem;
    bool itemsPickedChoose;
    bool vialsPickedChoose;
    public Image IconChangeImage;
    

    public void DisplayListPotions(int slotNum)
    {
        pannelChooseItem.SetActive(true);
        if (!itemsPickedChoose)
        {
            Clean();
            for (int i = 0; i < listItems.items.Count; i++)
            {
                if (listItems.items[i] != null && listItems.items[i] is PotionSO)
                {
                    var obj = Instantiate(slotList);
                    obj.transform.parent = slotList.transform.parent;
                    setComponentsInfoItem(i, obj);               
                    obj.SetActive(true);
                }
            }
            itemsPickedChoose = true;
            vialsPickedChoose = false;
        }
        SetSlotNumToCardList(slotNum);
    }

    public void DisplayListVials(int slotNum)
    {
        pannelChooseItem.SetActive(true);
        if (!vialsPickedChoose)
        {
            Clean();
            for (int i = 0; i < listItems.items.Count; i++)
            {
                if (listItems.items[i] != null && listItems.items[i] is VialsMetalsSO)
                {
                    var obj = Instantiate(slotList);
                    obj.transform.parent = slotList.transform.parent;
                    setComponentsInfoItem(i, obj);
                    obj.SetActive(true);
                }
            }
            itemsPickedChoose = false;
            vialsPickedChoose = true;
        }
        SetSlotNumToCardList(slotNum);
    }
    public void Clean()
    {
        int count = this.gameObject.transform.childCount;
        for ( int i=1; i<count; i++)
        {
             Destroy(gameObject.transform.GetChild(i).gameObject);
        }
    }
    void setComponentsInfoItem(int index, GameObject obj)
    {
        infoItem = obj.GetComponentsInChildren<TextMeshProUGUI>();
        infoItem[0].text = listItems.items[index].Name;
        infoItem[1].text = listItems.items[index].QuantityHeld.ToString() + "x";
        obj.GetComponent<EquipItem>().equipItems = equip;
        obj.GetComponent<EquipItem>().itemInfo = listItems.items[index];
        obj.GetComponent<EquipItem>().pannelChooseItem = pannelChooseItem;
    }
    void SetSlotNumToCardList(int slotNum)
    {
        var cardlist = GetComponentsInChildren<EquipItem>();
        for (int i = 0; i < cardlist.Length; i++)
        {
            cardlist[i].slotNum = slotNum;
        }
    }
    private void OnEnable()
    {
        //EquipItem.uiSpriteIconItem += ChangeIconUIEquipPanel;
    }
    private void OnDisable()
    {
        //EquipItem.uiSpriteIconItem -= ChangeIconUIEquipPanel;
    }
    void ChangeIconUIEquipPanel(Sprite icon)
    {
       transform.GetChild(0).GetComponent<Image>().sprite = icon;
    }
}