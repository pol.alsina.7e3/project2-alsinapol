using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class EquipItem : MonoBehaviour
{
    public ItemSo itemInfo;
    public ListItemsSO equipItems;
    public GameObject pannelChooseItem;
    public int slotNum;
    //public delegate void UISpriteIconItem(Sprite icon);
    public static event Action<ItemSo, int> uiSpriteIconItem = delegate { };
    private void OnEnable()
    {
        
    }
    void Start()
    {
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(EquipItemOnClick);
    }

    void EquipItemOnClick()
    {
        if (itemInfo is VialsMetalsSO)
        {
            equipItems.vialsEquip[slotNum] = (VialsMetalsSO)itemInfo;
            uiSpriteIconItem(itemInfo, 0);
        }
        else
        {
            equipItems.potionsEquip[slotNum] = (PotionSO)itemInfo;
            uiSpriteIconItem(itemInfo, 1);
        }

        pannelChooseItem.SetActive(false);
    }
}
