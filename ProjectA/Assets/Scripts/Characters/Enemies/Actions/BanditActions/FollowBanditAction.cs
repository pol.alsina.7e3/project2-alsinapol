using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBanditAction : Actions
{
    GameObject player;
    public LayerMask layerMaskPlayer;
    float speed;
    public override void StartState()
    {
        ChangeLayerAnimation(1, 1);
        player = GameObject.Find("Player");
        speed = this.GetComponent<Enemies>().speed;
    }
    public override void FinishState()
    {
       // StartCoroutine(ChangeLayerAnimationWaiting);
        ChangeLayerAnimation(1, 0);
    }
    public void FollowPlayer()
    {
        transform.LookAt(new Vector3(player.transform.position.x, this.transform.position.y, player.transform.position.z));
        transform.position = Vector3.MoveTowards(this.transform.position, new Vector3( player.transform.position.x,0f, player.transform.position.z), speed * Time.deltaTime);
        animator.SetFloat("VelocityY", transform.position.y);
        animator.SetFloat("VelocityX", transform.position.x);
    }
    /*IEnumerator ChangeLayerAnimationWaiting()
    {
        AnimatorClipInfo[] animationClipInfo = animator.GetCurrentAnimatorClipInfo(0);
       // yield return new WaitUntil(animationClipInfo[0].clip.)
    }*/
}
