using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBanditAction : Actions
{
    public Transform[] patrolPoints;
    [SerializeField] private float patrolSpeed;
    int currentTarget;
    float changeTargetDistance = 0.1f;
    public float timeToWait;
    float time;
    public override void StartState()
    {
        ChangeLayerAnimation(0, 1);
        
    }
    public override void FinishState()
    {
        ChangeLayerAnimation(0,0);
    }
    public void Patrol()
    {
        if (MoveToTarget())
        {         
            time += Time.deltaTime;
            if (time > timeToWait)
            {
                currentTarget = GetNextTarget();
                time = 0;
            }                   
        }

    }
    bool MoveToTarget()
    {
        Vector3 distanceVector = new Vector3(patrolPoints[currentTarget].position.x,0f, patrolPoints[currentTarget].position.z) - transform.position;
        if (distanceVector.magnitude < changeTargetDistance)
        {
            return true;
        }
        else
        {
            Vector3 velocityVector = distanceVector.normalized;
            transform.position += velocityVector * patrolSpeed * Time.deltaTime;
            transform.LookAt(new Vector3(patrolPoints[currentTarget].position.x, this.transform.position.y, patrolPoints[currentTarget].position.z));
            animator.SetFloat("VelocityY", velocityVector.z);
            animator.SetFloat("VelocityX", velocityVector.x);
            return false;
        }   
        
    }
    int GetNextTarget()
    {
        currentTarget++;
        if (currentTarget >= patrolPoints.Length)
        {
            currentTarget = 0;
        }
        return currentTarget;
    }

}
