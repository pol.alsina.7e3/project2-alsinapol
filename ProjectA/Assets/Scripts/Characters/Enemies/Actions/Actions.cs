using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actions : MonoBehaviour
{
    public Animator animator;
    protected virtual void Start()
    {
        animator = this.GetComponent<Animator>();
    }
    public virtual void StartState() { }
    public virtual void FinishState() { }
    protected void ChangeLayerAnimation(int indexLayer, float activate)
    {
        animator.SetLayerWeight(indexLayer, activate);
    }
}
