using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemies : Characters, IDamageble, IDropeable
{
    protected int currentHP;
    public HealthBar healthBar;
    public ScriptableState currentState;
    public ScriptableState die;
    //UnityEngine.AI.NavMeshAgent agent;
    public DropItemsSO dropItemEnemy;
    public int damage { get; set; }
    public DropItemsSO DropItemsSO { get => dropItemEnemy; }

    protected override void Start()
    {
        base.Start();
        currentHP = dataCharacter.maxHealth;
        healthBar.SetMaxHealt(dataCharacter.maxHealth);
    }
    protected virtual void Update()
    {
        currentState.action.OnUpdate(this);
        healthBar.LookAt();
    }
    public void StateTransition(ScriptableState state)
    {
        if (currentState.scriptableStateTransitions.Contains(state))
        {
            currentState.action.OnFinishedState(this);
            currentState = state;
            currentState.action.OnSetState(this);
        }
    }

    public void AplyDamage(int damageTaken)
    {
        int calculateDamage = damageTaken * damageTaken / (damageTaken + dataCharacter.Defense);
        currentHP -= calculateDamage;
        healthBar.SetHealth(currentHP);
        CheckHealth();
    }
    protected void CheckHealth()
    {
        if (currentHP <= 0)
        {
            StateTransition(die);
            DropItem();
        }
    }

    public void DropItem()
    {
        Debug.Log("Dropped");
        DropItemsSO.ItemDrop();
    }
}
