using UnityEngine;

public class BanditBehaviour : Enemies
{
    public ScriptableState patrol, follow, attack;
    bool isOnAttackRange;
    public float rangeAlert;
    public float rangeAttack;
    public LayerMask layerMaskPlayer;
    public LayerMask layerMaskGround;
    bool beAlert = false;
    bool grounded;
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        currentState = patrol;
        /*grounded = Physics.CheckSphere(this.transform.position, rangeAttack, layerMaskGround);
        rigidbody.mass = 99;
        rigidbody.drag = 99;*/
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        isOnAttackRange = Physics.CheckSphere(this.transform.position, rangeAttack, layerMaskPlayer);
        beAlert = Physics.CheckSphere(this.transform.position, rangeAlert, layerMaskPlayer);
        if (beAlert)
        {
            if (isOnAttackRange)
            {

                StateTransition(attack);
            }
            else
            {
                StateTransition(follow);
            }
            
        }
        else
        {
            StateTransition(patrol);
        }
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, rangeAlert);
        Gizmos.DrawWireSphere(transform.position, rangeAttack);
    }
    
}
