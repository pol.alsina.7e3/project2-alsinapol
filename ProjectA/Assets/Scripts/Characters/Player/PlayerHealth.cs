using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static PlayerHealth;

public class PlayerHealth : MonoBehaviour , IDamageble
{
    public DataCharacterSO playerData;
    public HealthBar healthBar;
    private int currentHP;
    public delegate void HitBloodUIDmg(int health);
    public static event HitBloodUIDmg hitBloodUI;

    public int damage { get; set; }

    void Start()
    {
        currentHP = playerData.maxHealth;
        healthBar.SetMaxHealt(playerData.maxHealth);
    }
    private void CheckHealth()
    {
        if (currentHP <= 0)
        {
            //Reload scene
        }
    }
    public void AplyDamage(int damageTaken)
    {    
        currentHP -= damageTaken * damageTaken / (damageTaken + playerData.Defense);
        healthBar.SetHealth(currentHP);
        CheckHealth();
    }
    void UpdateUIHealth()
    {
        healthBar.SetHealth(currentHP);
    }
    public void RestoreHealth(int percentatgeOfHealing)
    {
        int restoreHealth = (percentatgeOfHealing/playerData.maxHealth) * 100;
        
        if (currentHP + restoreHealth > playerData.maxHealth)
        {
            currentHP = playerData.maxHealth;
        }
        else
        {
            currentHP += restoreHealth;
        }
        UpdateUIHealth();
    }
}
