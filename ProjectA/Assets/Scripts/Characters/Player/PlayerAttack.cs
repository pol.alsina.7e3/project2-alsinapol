using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    DataCharacterSO playerData;
    [SerializeField] private float attackSpeed = 1f; // Velocidad del ataque
    [SerializeField] private float attackDuration = 0.5f; // Duraci�n del ataque

    private bool isAttacking = false; // Estado del ataque
    private float attackTimer = 0f; // Temporizador para controlar la velocidad del ataque

    private void Start()
    {
        playerData = GetComponent<PlayerBehaviour>().dataCharacter;
    }
    // Funci�n para iniciar el ataque
    public void StartAttack()
    {
        isAttacking = true;
        attackTimer = 0f;
    }

    // Funci�n para detener el ataque
    public void StopAttack()
    {
        isAttacking = false;
    }

    // Funci�n para verificar si el objeto est� atacando
    public bool IsAttacking()
    {
        return isAttacking;
    }

    // Funci�n para controlar el temporizador del ataque
    private void Update()
    {
        if (isAttacking)
        {
            attackTimer += Time.deltaTime;
            if (attackTimer >= attackDuration)
            {
                StopAttack();
                attackTimer = 0f;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("fora if");
        IDamageble damageble = other.gameObject.GetComponent<IDamageble>();
        if (isAttacking && damageble != null)
        {
            Debug.Log("dintre if");
            damageble.AplyDamage(playerData.Attack);            
        }
    }
}
