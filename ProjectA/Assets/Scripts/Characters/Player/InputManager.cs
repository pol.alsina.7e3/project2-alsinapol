using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;
    public MapInputPlayer inputs;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        inputs = new MapInputPlayer();
        inputs.GamePlay.Enable();
    }
    public Vector2 GetMovementPlayer()
    {
        return inputs.GamePlay.Move.ReadValue<Vector2>();
    }

    public bool GetRunPressed()
    {
        return inputs.GamePlay.Run.IsPressed();
    }
    public bool GetJumpButtonPressed()
    {
        return inputs.GamePlay.Jump.WasPressedThisFrame();
    }
    public bool GetAttackButton()
    {
        return inputs.GamePlay.Attack.WasPressedThisFrame();
    }
    //Habilities controlls
    //Habilities steel controlls
    public void EnableSteelControllers()
    {
        inputs.SteelController.Enable();
    }
    public bool ButtonHabilitySteelIsActioned()
    {
        return inputs.SteelController.UseHability.IsPressed();
    }
    public void DisableSteelControllers()
    {
        inputs.SteelController.Disable();
    }
}
