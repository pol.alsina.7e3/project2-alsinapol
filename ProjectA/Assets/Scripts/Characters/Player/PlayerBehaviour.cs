using UnityEngine;

public class PlayerBehaviour : Characters
{
    [SerializeField] float _speedSprint;
    float targetSpeed = 0;

    [Header("Jump")]
    [SerializeField] private float jumpForce;
    private bool isGrounded;
    private Vector3 jump;
    public float JumpTimeout = 0.50f;
    public float FallTimeout = 0.15f;

    public Transform cam;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    Vector2 movementInput;
    public Vector3 moveDir;

    [Header("AttackMelee")]
    private PlayerAttack playerAttack;
    bool isAttacking;
    string attackAnimationTrigger = "Attack";

    private void Awake()
    {
       playerAttack = GetComponent<PlayerAttack>();
    }
    protected override void Start()
    {
        base.Start();
        jump = new Vector3(0.0f, jumpForce, 0.0f);
    }

    void Update()
    {
        PlayerMovement();
        Jump();
        Animations();
        Attack();
    }

    void PlayerMovement()
    {
        targetSpeed = InputManager.Instance.GetRunPressed() ? _speedSprint : speed;
        if (InputManager.Instance.GetMovementPlayer() == Vector2.zero) targetSpeed = 0f; 
        movementInput = InputManager.Instance.GetMovementPlayer();      
        Vector3 direction = new Vector3(movementInput.x, 0, movementInput.y);
        //rigidbody.velocity =Vector3.up*rigidbody.velocity.y+ direction * targetSpeed * Time.deltaTime;
        movementInput.Normalize();  
        if (movementInput != Vector2.zero)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity,turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, rotation, 0f);
            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            
        }
        else
        {
            targetSpeed = 0f;
        }
        rigidbody.velocity = Vector3.up * rigidbody.velocity.y + moveDir * targetSpeed * Time.deltaTime;
    }

   void Animations()
    {
        animator.SetFloat("InputX", movementInput.x);
        animator.SetFloat("InputY", movementInput.y);
        animator.SetBool("IsInAir", !isGrounded);
        animator.SetFloat("Velocity", targetSpeed);
    }
    void Jump()
    {
        if (isGrounded)
        {
            if (InputManager.Instance.GetJumpButtonPressed())
            {
                rigidbody.velocity += jump;
                isGrounded = false;
            }            
        }        
    }

    void Attack()
    {
        if (InputManager.Instance.GetAttackButton())
        {
            // Verificar si el jugador ya est� atacando
            if (!isAttacking)
            {
                // Iniciar el ataque
                isAttacking = true;
                playerAttack.StartAttack();
                animator.SetTrigger(attackAnimationTrigger); // Activar la animaci�n de ataque
            }
        }
        if (isAttacking)
        {
            if (playerAttack.IsAttacking())
            {
                if (animator.GetCurrentAnimatorStateInfo(0).IsName(attackAnimationTrigger) &&
                    animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                {
                    isAttacking = false;
                    playerAttack.StopAttack(); // Detener el ataque
                }
            }
            else
            {
                isAttacking = false;
            }
        }
    }


    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
        
    }
    private void OnCollisionExit(Collision collision)
    {

        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
}
