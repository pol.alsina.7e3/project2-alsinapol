using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ActionItem : MonoBehaviour
{
    public ItemSo item;
    public abstract void ItemAction(ItemSo item);
}
