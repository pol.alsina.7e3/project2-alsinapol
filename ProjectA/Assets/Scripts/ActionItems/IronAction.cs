using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronAction : ActionItem
{
    public override void ItemAction(ItemSo item)
    {
        this.item = (VialsMetalsSO)item;
        gameObject.AddComponent<IronBehaviour>();
        StartCoroutine(Cooldown());
    }
    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(item.durantionEffect);
        GetComponent<IronBehaviour>().DestroyComponent();
    }
}
