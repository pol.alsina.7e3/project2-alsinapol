using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TinAction : ActionItem
{
    VialsMetalsSO vials;
    public override void ItemAction(ItemSo item)
    {
        vials = (VialsMetalsSO)item;
        gameObject.AddComponent<TinBehaviour>().SetRangVision(vials.rangVision);
        StartCoroutine(Cooldown());
    }
    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(vials.durantionEffect);
        GetComponent<TinBehaviour>().DestroyComponent();
    }
}
