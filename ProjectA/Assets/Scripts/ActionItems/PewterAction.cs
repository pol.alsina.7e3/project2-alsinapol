using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PewterAction : ActionItem
{
    VialsMetalsSO vials;
    public override void ItemAction(ItemSo item)
    {
        vials = (VialsMetalsSO)item;
        gameObject.AddComponent<PewterBehaviour>().SetPercentatgeToBuffStats(vials.percentatgeToBuffStats);
        StartCoroutine(Cooldown());
    }
    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(vials.durantionEffect);
        GetComponent<PewterBehaviour>().DestroyComponent();
    }
}
