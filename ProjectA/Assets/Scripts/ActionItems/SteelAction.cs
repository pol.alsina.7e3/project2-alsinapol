using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteelAction : ActionItem
{
    
    public override void ItemAction(ItemSo item)
    {
        this.item = item;
        gameObject.AddComponent<SteelBehaviour>();
        StartCoroutine(Cooldown());
    }
    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(item.durantionEffect);
        GetComponent<SteelBehaviour>().DestroyComponent();
    }
}
