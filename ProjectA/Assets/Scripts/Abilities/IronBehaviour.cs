using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronBehaviour : AbilityBehaviour
{
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, metalsLayer))
        {
            //activate targeteable crosshair?
            if (InputManager.Instance.ButtonHabilitySteelIsActioned())
            {
                var massMetal = hit.rigidbody.mass;
                var gO = hit.rigidbody.gameObject;
                if (massMetal > playerData.mass)
                {
                    var step = playerData.powerAllomantic * Time.deltaTime;
                    gO.transform.position = Vector3.MoveTowards(gO.transform.position, this.transform.position, step);
                }
                else
                {
                    var step = massMetal * Time.deltaTime;
                    gO.transform.position = Vector3.MoveTowards(this.transform.position, gO.transform.position, step);                  
                }
            }
        }
    }
}
