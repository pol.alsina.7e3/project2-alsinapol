using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteelBehaviour : AbilityBehaviour
{
    private void Start()
    {
        InputManager.Instance.EnableSteelControllers();
    }
    void Update()
    {
        Debug.Log("hola estic al update");
        RaycastHit hit;
        Debug.DrawRay(new Vector3(0,transform.position.y,0), transform.TransformDirection(Vector3.forward), Color.red);
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, metalsLayer))
        {
            Debug.Log("Dintre if raycast");
            //activate targeteable crosshair?
            if (InputManager.Instance.ButtonHabilitySteelIsActioned())
            {
                Debug.Log("Activo Habilitat");
                var massMetal = hit.rigidbody.mass;
                var gO = hit.rigidbody.gameObject;
                if (massMetal < playerData.mass)
                {
                    var step = playerData.powerAllomantic * Time.deltaTime;
                    gO.transform.position = Vector3.MoveTowards(this.transform.position,gO.transform.position, step);
                }
                else
                {
                    var step = massMetal * Time.deltaTime;
                    gO.transform.position = Vector3.MoveTowards(gO.transform.position, this.transform.position, step);
                }
            }
        }
    }
    private void OnDestroy()
    {
        InputManager.Instance.DisableSteelControllers();
    }


}
