using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PewterBehaviour : AbilityBehaviour
{
    int percentatgeToBuffStats;
    DataCharacterSO DataCharacterSO;
    int attack, defense, powerAllomantic;
    public void SetPercentatgeToBuffStats(int buff)
    {
        percentatgeToBuffStats = buff;
    }
    void Start()
    {
        DataCharacterSO = GetComponent<PlayerBehaviour>().dataCharacter;
        attack = DataCharacterSO.Attack;
        defense = DataCharacterSO.Defense;
        powerAllomantic = DataCharacterSO.powerAllomantic;
        DataCharacterSO.Attack += BuffStat(attack);
       DataCharacterSO.Defense += BuffStat(defense);
       DataCharacterSO.powerAllomantic += BuffStat(powerAllomantic);
    }
    int BuffStat(int StatToBuff)
    {
        return (percentatgeToBuffStats / StatToBuff) * 100;
    }
    private void OnDestroy()
    {
        DataCharacterSO.Attack = attack;
        DataCharacterSO.Defense = defense;
        DataCharacterSO.powerAllomantic = powerAllomantic;
    }
}
