using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityBehaviour : MonoBehaviour
{
    protected LayerMask metalsLayer;
    protected GameObject player;
    protected DataCharacterSO playerData;

    void Start()
    {
        metalsLayer = 6;
        playerData = GetComponent<PlayerBehaviour>().dataCharacter;
    }

    public void DestroyComponent()
    {
        Destroy(this);
    }

}
