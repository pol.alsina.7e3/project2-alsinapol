using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOVAbilities : MonoBehaviour
{
    LayerMask metalLayer = 6;
    float visionRadius = 10;
    float visionAngle = 90;
    public LayerMask obstacleMask;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FoV()
    {
        Collider[] ObjesctsInsideTheRadius = Physics.OverlapSphere(transform.position, visionRadius, metalLayer);

        if (ObjesctsInsideTheRadius.Length != 0)
        {
            /*The only thing the OverlapShpere can find is the player, as we are searching
             for the Player layerMask. So always we would want to pick the [0]*/
            for (int i = 0; i < ObjesctsInsideTheRadius.Length; i++)
            {
                Transform target = ObjesctsInsideTheRadius[i].transform;

                /*If we have found the player, we create a Vector3 with the direction towards it.
                 We would use it to check if there is something between them*/
                Vector3 targetDirection = (target.position - transform.position).normalized;

                if (Vector3.Angle(transform.forward, targetDirection) < visionAngle / 2)
                {
                    /*If the player is inside the radious range AND the angle range we throw
                     a ray towards the player. If it collides with something tagged "Obstacle" it means
                    there is something that blocks the vision and the enemy can't see the player*/
                    float distanceToTarget = Vector3.Distance(transform.position, target.position);
                    if (!Physics.Raycast(transform.position, ObjesctsInsideTheRadius[i].transform.position, distanceToTarget, obstacleMask))
                    {

                    }
                    


                }
            }
            

        }
    }
}
