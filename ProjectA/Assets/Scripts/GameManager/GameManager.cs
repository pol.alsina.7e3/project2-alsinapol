using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public ListItemsSO inventory;
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

   public GameObject GetPlayer()
    {
        return GameObject.Find("Player");
    }
    public ListItemsSO GetInventoryPlayer()
    {
        return inventory;
    }
}
