using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DictionaryProva : MonoBehaviour
{
    public List<ItemSo> llista;
    public List<AbilityBehaviour> components;
    public static Dictionary<ItemSo, AbilityBehaviour> relationItemComponent = new Dictionary<ItemSo, AbilityBehaviour>();
    private void Awake()
    {
        for (int i = 0; i < llista.Count; i++)
            relationItemComponent.Add(llista[i], components[i]);
    }
}
