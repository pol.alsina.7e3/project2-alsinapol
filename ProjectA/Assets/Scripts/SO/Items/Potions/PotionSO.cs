using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PotionSO", menuName = "ItemsSO/Potions")]
public class PotionSO : ItemSo
{
    public int healthRestore;

    public override void ItemEffectExecute()
    {
        Debug.Log("Prova ItemSo");
       var player = GameManager.Instance.GetPlayer();
        var maxHealth = player.GetComponent<PlayerBehaviour>().dataCharacter.maxHealth;
        healthRestore = (healthRestore * 100) / maxHealth; 
       player.GetComponent<PlayerHealth>().RestoreHealth(healthRestore);
    }
}
