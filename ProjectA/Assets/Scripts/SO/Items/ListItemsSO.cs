using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ListItemsSO", menuName = "ItemsSO/ListItemsSO")]
public class ListItemsSO : ScriptableObject
{
   public List<ItemSo> items;
   public List<PotionSO> potionsEquip;
   public List<VialsMetalsSO> vialsEquip;
   public int Empyria; //name of the money in game
    public delegate void UIUpdateEmpyria(int empyria);
    public static event UIUpdateEmpyria uiUpdateEmpyria;
    public void AddEmpyria(int money)
    {
        Empyria += money;
        uiUpdateEmpyria(Empyria);
    }
    public void ExtractEmpyria(int money)
    {
        Empyria -= money;
        uiUpdateEmpyria(Empyria);
    }
    public void AddItem(ItemSo item)
    {
        bool itemIsInList = false;
        for (int i = 0; i < items.Count; i++)
        {
            if (item == items[i])
            {
                items[i].QuantityHeld++;
                itemIsInList = true;
            }
        }
        if (!itemIsInList)
        {
            item.QuantityHeld++;
            items.Add(item);
        }
    }
}
