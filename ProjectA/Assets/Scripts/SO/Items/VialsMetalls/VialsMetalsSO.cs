using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "VialsMetals", menuName = "ItemsSO/VialsMetals")]
public class VialsMetalsSO : ItemSo
{
    public Image iconHudd;
    public int percentatgeToBuffStats;
    public int rangVision;
    public override void ItemEffectExecute()
    {
        if (QuantityHeld > 0)
        {
            GameObject player = GameManager.Instance.GetPlayer();
            foreach (KeyValuePair<ItemSo, AbilityBehaviour> kvp in DictionaryProva.relationItemComponent)
            {
                /*if (kvp.Key == this)
                {
                    player.GetComponent<SteelAction>().ItemAction(this);
                }*/
                switch (kvp.Key.Name)
                {
                    case "Vial Steel (S)":
                    case "Vial Steel (M)":
                    case "Vial Steel(L)":
                            player.GetComponent<SteelAction>().ItemAction(this);
                        break;
                    case "Vial Pewter (L)":
                    case "Vial Pewter (M)":
                    case "Vial Pewter (S)":
                        player.GetComponent<PewterAction>().ItemAction(this);
                        break;
                    case "Vial Iron (L)":
                    case "Vial Iron (M)":
                    case "Vial Iron (S)":
                        player.GetComponent<IronAction>().ItemAction(this);
                        break;
                    case "Vial Tin (L)":
                    case "Vial Tin (M)":
                    case "Vial Tin (S)":
                        player.GetComponent<TinAction>().ItemAction(this);
                        break;
                    default:
                        break;
                }

            }
        }
        
    }
}