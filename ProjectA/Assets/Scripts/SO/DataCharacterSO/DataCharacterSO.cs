using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataCharacterSO : ScriptableObject
{
    public new string name;
    public string description;
    [Header("Stats Character")]
    public int maxHealth;
    public int Attack;
    public int Defense;
    public int powerAllomantic;
    public int mass;
    [Header("Expirence")]
    public int currentLevel;
}
