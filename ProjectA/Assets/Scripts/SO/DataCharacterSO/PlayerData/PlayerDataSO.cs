using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerDataSO", menuName = "ScriptableObjects/PlayerDataSO", order = 1)]
public class PlayerDataSO : DataCharacterSO
{
    public int experienceLeftToLevelUp;
    private int levelMax = 99;
    public int experience;
    [Header("Extra Points")]
    public int habilityPoints;
    public int statsPoints;
    public void PlusExpGetted(int xp)
    {
        experience += xp;
        CheckLevelUp();
    }
    void CheckLevelUp()
    {
        if (experience >= experienceLeftToLevelUp)
        {
            if (currentLevel <= levelMax)
            {
                currentLevel++;
                CalculateExpirienceNeedNextLevel();
                statsPoints++;
                habilityPoints++;
                experience = 0;
            }
        }
    }
    void CalculateExpirienceNeedNextLevel()
    {
        int xp = (int)(0.8807 * Mathf.Pow(currentLevel,2) + 22.024 * currentLevel + 66.793);
        xp = (int)Mathf.Floor((float)xp);
        experienceLeftToLevelUp += xp;
    }
    public void BoostHp(int boost)
    {
        int boostTotal = 10 * boost;
        maxHealth += boostTotal;
    }
    public void BoostAttack(int boost)
    {
        Attack += boost;
    }
    public void BoostDefense(int boost)
    {
        Defense += boost;
    }
    public void BoostAP(int boost)
    {
        powerAllomantic += boost;
    }
}
