using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DropItemsSO", menuName = "DropItemsSO")]
public class DropItemsSO : ScriptableObject
{
    public ItemSo[] vialsDrop, potionsDrop;
    public int exp;
    public int empyriaDropMax, empyriaDropMin;
    public int probVials, probPotions;
    public ListItemsSO inventory;
    public PlayerDataSO playerData;
    public delegate void DroppedItemUIShow(string droppedItem);
    public static event DroppedItemUIShow droppedItemUIShow;
    public void ItemDrop()
    {
        int probItemDrop = Random.Range(0, 100);
        int empyria = Random.Range(empyriaDropMin, empyriaDropMax);
        playerData.PlusExpGetted(exp);
        droppedItemUIShow(exp.ToString() + " Xp");
        inventory.AddEmpyria(empyria);
        droppedItemUIShow(empyria.ToString() + " E");
        if (probVials <= probItemDrop)
        {
            //check de prob of the wich vial will drop
            inventory.AddItem(vialsDrop[0]);
            droppedItemUIShow(vialsDrop[0].Name + " x1");
        }
        if (probPotions <= probItemDrop)
        {
            //check de prob of the wich potion will drop
            inventory.AddItem(potionsDrop[0]);
            droppedItemUIShow(potionsDrop[0].Name + " x1");
        }
        
    }
}
