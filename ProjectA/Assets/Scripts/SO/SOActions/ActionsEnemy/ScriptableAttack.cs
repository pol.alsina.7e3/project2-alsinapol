using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptableAttack", menuName = "ScriptableObjects2/ScriptableAction/ScriptableAttack")]
public class ScriptableAttack : ScriptablesActions
{
    private Enemies finish;
    public override void OnFinishedState(Enemies sc)
    {
        sc.GetComponent<AttackBanditAction>().FinishState();
    }

    public override void OnSetState(Enemies sc)
    {    
        finish = sc.GetComponent<Enemies>();
        sc.GetComponent<AttackBanditAction>().StartState();
    }

    public override void OnUpdate(Enemies sc)
    {
        sc.GetComponent<AttackBanditAction>().UpdateState();
    }
}
