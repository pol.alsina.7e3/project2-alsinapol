using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptableDie", menuName = "ScriptableObjects2/ScriptableAction/ScriptableDie")]

public class ScriptableDie : ScriptablesActions
{
    public override void OnFinishedState(Enemies sc)
    {
        //TODO
    }

    public override void OnSetState(Enemies sc)
    {
        sc.GetComponent<DieBanditAction>().StartState();
    }

    public override void OnUpdate(Enemies sc)
    {
        
    }
}
