using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptablePatrol", menuName = "ScriptableObjects2/ScriptableAction/ScriptablePatrol")]
public class ScriptablePatrol : ScriptablesActions
{
    public override void OnFinishedState(Enemies sc)
    {
        sc.GetComponent<PatrolBanditAction>().FinishState();
    }

    public override void OnSetState(Enemies sc)
    {
        sc.GetComponent<PatrolBanditAction>().StartState();
        Debug.Log("OnSet");

    }

    public override void OnUpdate(Enemies sc)
    {
        sc.GetComponent<PatrolBanditAction>().Patrol();

    }
}
