using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDropeable
{
    public DropItemsSO DropItemsSO { get; }
    void DropItem();
}
